﻿USE ciclistas;

/*HOJA2-----------------------------------------------------------------------------*/


/**.1- Nº de ciclistas que hay.**/
  SELECT COUNT(*) 
  FROM  ciclista c;
/**.2- Nº de ciclistas que hay en el equipo Banesto.**/
  SELECT COUNT(*) 
    FROM ciclista c
    WHERE c.nomequipo='Banesto'; 
/**.3- La edad media de los ciclistas**/
  SELECT AVG(c.edad) 
    FROM ciclista c;
/**.4- La edad media de los ciclistas de Banesto.**/ 
    SELECT AVG(c.edad) 
      FROM ciclista c
      WHERE c.nomequipo='Banesto';
/**.5- La edad media de los ciclistas por cada equipo**/
    SELECT c.nomequipo, AVG(c.edad) 
      FROM ciclista c
      GROUP BY c.nomequipo;
/**.6- El nº de ciclistas por equipo. **/

SELECT COUNT(*),c.nomequipo
  FROM ciclista c
  GROUP BY c.nomequipo; 

/**.7- El nº total de puertos. **/
SELECT COUNT(*)
  FROM puerto p;

/**.8- El nº total de puertos mayores de 1500 **/
  SELECT COUNT(*) 
    FROM puerto p
    WHERE p.altura>1500;

/**.9- listar el nombre de los equipos 
  que tengan más de 4 ciclistas.**/

-- con HAVING
SELECT COUNT(*) numciclistas,nomequipo 
  FROM ciclista c
  GROUP BY c.nomequipo
  HAVING numciclistas>4;
  -- ciclistas por equipo (c1)
  SELECT 
    c.nomequipo, COUNT(*)num_ciclistas 
  FROM 
    ciclista c 
  GROUP BY c.nomequipo;

  -- completa
  SELECT 
    c1.nomequipo 
  FROM (
    SELECT 
      c.nomequipo, COUNT(*)num_ciclistas 
    FROM 
      ciclista c 
    GROUP BY c.nomequipo
    ) c1
  WHERE 
    c1.num_ciclistas>4;


/**.10- Listar el nombre de los equipos 
  que tengan más de cuatro ciclistas 
  cuya edad esté entre 28 y 32. **/
  -- ciclistas por equipo (c1)
  SELECT 
    c.nomequipo, COUNT(*)num_ciclistas 
  FROM 
    ciclista c 
  WHERE 
    c.edad BETWEEN 28 AND 32
  GROUP BY c.nomequipo;

  -- completa
  SELECT 
    c1.nomequipo 
  FROM (
    SELECT 
      c.nomequipo, COUNT(*)num_ciclistas 
    FROM 
      ciclista c 
    WHERE 
      c.edad BETWEEN 28 AND 32
    GROUP BY c.nomequipo
    ) c1
  WHERE 
    c1.num_ciclistas>4;
  
  -- con HAVING
  SELECT c.nomequipo
    FROM ciclista c
    WHERE c.edad BETWEEN 28 AND 32
    GROUP BY c.nomequipo
    HAVING COUNT(*)>4;
  
/**.11- Indícame el número de etapas 
  que ha ganado cada uno de los ciclistas.**/
    
  SELECT c.dorsal,COUNT(*)
    FROM etapa e
    GROUP BY c.dorsal;

/**.12- Indícame el dorsal de los ciclistas que hayan ganado más de 1 etapa
**/
-- victorias de cada ciclista (c1)
  SELECT 
    e.dorsal, COUNT(*)num_etapas 
  FROM 
    etapa e 
  GROUP BY e.dorsal;

  -- completa
  SELECT 
    c1.dorsal 
  FROM (
    SELECT 
      e.dorsal, COUNT(*)num_etapas 
    FROM 
      etapa e 
    GROUP BY e.dorsal
    ) c1
  WHERE 
    c1.num_etapas>1;

-- con HAVING
SELECT e.dorsal 
  FROM etapa e
  GROUP BY e.dorsal
  HAVING COUNT(*)>1;

/*--------------------------------------------------------------------------------
/*
/** La altura media de los puertos por etapa 
  SELECT p.numetapa, AVG(p.altura), COUNT(*)
    FROM puerto p 
    GROUP BY p.numetapa;

  
  SELECT AVG(p.altura),p.categoria
    FROM puerto p
    GROUP BY p.categoria
    HAVING AVG(p.altura)>2000;
    

  SELECT AVG(p.altura),media,p.categoria
    FROM puerto p
    GROUP BY p.categoria
    HAVING media>2000;

  SELECT p.categoria
    FROM puerto p
    GROUP BY p.categoria
    HAVING AVG(p.altura)>2000;

  SELECT p.categoria,AVG(p.altura),COUNT(*)
    FROM puerto p
    WHERE p.altura>1500
    GROUP BY p.categoria
    HAVING AVG(p.altura)>2000;
    
  SELECT * FROM (
  SELECT p.categoria,AVG(p.altura),media,COUNT(*) numero
    FROM puerto p
    WHERE p.altura>1500
    GROUP BY p.categoria) c1
    WHERE (c1.media02)>2000;
  **/
*/